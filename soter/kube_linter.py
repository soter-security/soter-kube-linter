"""
Module providing the ASGI app for soter-kube-linter.
"""

import asyncio
import json
import logging
import os
import re
import shlex

import yaml

from quart import Quart

from jsonrpc.model import JsonRpcException

from jsonrpc.server import Dispatcher
from jsonrpc.server.adapter.quart import websocket_blueprint

from .scanner.models import ScannerStatus, Resource, Severity, ConfigurationIssue


SCANNER_KIND = 'KubeLinter'
SCANNER_VENDOR = 'StackRox'


# Configuration options
#: The kube-linter command to use
KUBE_LINTER_COMMAND = os.environ.get('KUBE_LINTER_COMMAND', 'kube-linter')
#: The number of concurrent scans to allow per worker
KUBE_LINTER_CONCURRENT_SCANS = int(os.environ.get('KUBE_LINTER_CONCURRENT_SCANS', '1'))


class KubeLinterError(JsonRpcException):
    """
    Raised when there is an error calling out to the Grype CLI.
    """
    message = "KubeLinter error"
    code = 100


# Build the Quart app
app = Quart(__name__)
# Register the JSON-RPC blueprint
dispatcher = Dispatcher()
app.register_blueprint(websocket_blueprint(dispatcher), url_prefix = '/')


logger = logging.getLogger(__name__)


@dispatcher.register
async def status():
    """
    Return status information for the scanner.
    """
    # Just get the kube-linter version
    proc = await asyncio.create_subprocess_shell(
        f"{KUBE_LINTER_COMMAND} version",
        stdout = asyncio.subprocess.PIPE,
        stderr = asyncio.subprocess.PIPE
    )
    stdout_data, stderr_data = await proc.communicate()
    if proc.returncode == 0:
        return ScannerStatus(
            kind = SCANNER_KIND,
            vendor = SCANNER_VENDOR,
            version = stdout_data.strip(),
            available = True,
            message = "available"
        )
    else:
        logger.error('kube-linter command failed: {}'.format(stderr_data.decode()))
        return ScannerStatus(
            kind = SCANNER_KIND,
            vendor = SCANNER_VENDOR,
            version = 'unknown',
            available = False,
            message = 'could not detect status'
        )


@app.before_serving
async def create_semaphore():
    """
    Create a semaphore that we will use to limit concurrency of scanning
    """
    app.scan_semaphore = asyncio.Semaphore(KUBE_LINTER_CONCURRENT_SCANS)


# The regex to use for parsing lines
KUBE_LINTER_ISSUE_RE = re.compile(
    r"^<standard input>: "
    r"\(object: (?P<namespace>.+)/(?P<name>[\w-]+) (?P<version>[\w/]+), Kind=(?P<kind>\w+)\) "
    r"(?P<description>.*) "
    r"\(check: (?P<check>[\w-]+), remediation: (?P<remediation>.*)\)$"
)


@dispatcher.register
async def scan_resources(resources):
    """
    Scan the given resources and return any issues.
    """
    # Make sure only a limited number of kube-linter instances can run at once
    async with app.scan_semaphore:
        # Define kube-linter process
        # We will send each resource to kube-linter using stdin, so we run the lint
        # process with with "-" as the filename
        proc = await asyncio.create_subprocess_shell(
            f"{KUBE_LINTER_COMMAND} lint -",
            stdin = asyncio.subprocess.PIPE,
            stdout = asyncio.subprocess.PIPE,
            stderr = asyncio.subprocess.PIPE
        )
        # Send the data to stdin as multiple YAML resources
        stdin_data = yaml.safe_dump_all(
            resources,
            explicit_start = True,
            default_flow_style = False
        )
        stdout_data, stderr_data = await proc.communicate(stdin_data.encode())
    # kube-linter returns all output using stderr
    # It doesn't support JSON output, so we need to parse the terminal format
    # It also exits with an exit status of 1 for all errors or if it find any issues
    issue_data = stderr_data.decode()
    # If the output starts with "<standard input>", assume the command ran successfully
    if issue_data.startswith("<standard input>"):
        issues = []
        for issue_line in issue_data.splitlines():
            match = KUBE_LINTER_ISSUE_RE.match(issue_line)
            # Ignore lines that don't match
            if match is None:
                continue
            # Convert the issue to a Soter configuration issue
            # kube-linter uses "<no namespace>" if there was no namespace in the config
            namespace = match.group('namespace')
            if namespace == "<no namespace>":
                namespace = None
            issues.append(
                ConfigurationIssue(
                    title = match.group('description'),
                    severity = Severity.HIGH,
                    suggested_remediation = match.group('remediation'),
                    affected_resources = {
                        Resource(
                            kind = match.group('kind'),
                            name = match.group('name'),
                            # If no namespace was given, it must be omitted, not given as None
                            **(dict(namespace = namespace) if namespace else {})
                        )
                    }
                )
            )
        return issues
    else:
        raise KubeLinterError(issue_data)
